# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.0.1](https://gitlab.com/notejam/infrastructure/compare/v1.0.0...v1.0.1) (2020-05-19)


### Bug Fixes

* missing terraform init on deploy stage ([7caed19](https://gitlab.com/notejam/infrastructure/commit/7caed19201415741e8b96cfaf3b287ed82b8b92c))
* change wrong ci template version ([4556148](https://gitlab.com/notejam/infrastructure/commit/455614881a1a90ffb4817d9c6833a39c84c95e34))

## 1.0.0 (2020-05-19)


### Features

* add database ([6f90df6](https://gitlab.com/notejam/infrastructure/commit/6f90df6227dd38d2bd729aabb8c8d379c681ef7b))
* add database in the instance ([85072d0](https://gitlab.com/notejam/infrastructure/commit/85072d09dc16ab9ed27c991cee752497557d2a82))
* add gke cluster and vpc ([2b77a9e](https://gitlab.com/notejam/infrastructure/commit/2b77a9e28bb2e9a08c22816f511636c17dc20c02))


### Bug Fixes

* decrease address_count ([96c7bf8](https://gitlab.com/notejam/infrastructure/commit/96c7bf8371d6de857cd8cadefe063d6d9e3bccb1))
* disable cluster autoscaler ([8d79d60](https://gitlab.com/notejam/infrastructure/commit/8d79d606c6bb666f94cd18a54e985196c04526eb))
* move some configuration as variables ([a6e7cf7](https://gitlab.com/notejam/infrastructure/commit/a6e7cf762986899f34c73815bdec1c14be70fe40))
* remove unused variable ([2b3ced3](https://gitlab.com/notejam/infrastructure/commit/2b3ced3cd66ecabb140e84ffb58542e8f1771f6f))
