# IaC Infrastructure

![pipeline](https://gitlab.com/notejam/infrastructure/badges/master/pipeline.svg)

Infrastructure as code repository with Google Kubernetes Engine and Cloud SQL for [notejam application](https://gitlab.com/notejam/application).

This repository contains the following:

- VPC implementation with one subnetwork for GKE and the needed firewall rules.
- Google Kubernetes Engine with one node pool.
- Cloud SQL instance with one database inside.

## Requirements

Notejam IaC requires the following tools:

- [Terraform](https://terraform.io) >= 0.12
- [Kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl) >= v1.15
- [Google Cloud SDK](https://cloud.google.com/sdk/docs/quickstarts) >= v290.0

## Project configuration

Notejam infrastructure requires the variables to be present in `platform/terraform.tfvars` file. The following variables must be specified:

| Variable | Purpose |
| --- | --- |
| `project` | Google Cloud project to use |
| `region` | Google region where to create the infrastructure  |
| `zone` | Google zone where to create the infrastructure |
| `vpn_cidr` | Trusted addresses to access the network in cidr format |
| `database_password` | Password user for the MySQL server |

Add the json key of a service account with admin capability on the project in `account.json` file, see: [Creating and managing service account keys](https://cloud.google.com/iam/docs/creating-managing-service-account-keys).

## On the first time running

On the first run you need to download the terraform module and the providers, run `terraform init`

If you want to use your own backend configuration you can pass `-backend-config` flag to the init command, for example:

```
terraform init -backend-config="bucket=notejam-operations"
```

See more information about it in the [official documentation](https://www.terraform.io/docs/backends/config.html#partial-configuration).

## Validate new code

When you finish writing new modules, make sure that it is correct syntax by running `terraform validate`.

## Run terraform locally

Check the infrastructure status with `terraform plan` command then run `terraform apply` command.

## Note

This is a proof of concept of an infrastructure for Notejam application. It is lacking in certain features:

- Not fully secure: Database user is configured without a restricted host.
- Missing DNS: All endpoints are configured without a DNS.
- Missing HA: The infrastructure is not in HA mode, for both GKE and Cloud SQL.
