output "cluster" {
  value = module.cluster.name
}

output "master_endpoint" {
  value = module.cluster.endpoint
}

output "address" {
  value = module.nat.address
}

output "address_count" {
  value = module.nat.address_count
}
