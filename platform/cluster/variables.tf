variable "region" {
  description = "Cluster region"
}

variable "zone" {
  description = "Cluster zone"
}

variable "network" {
  description = "Cluster network"
}

variable "subnetwork" {
  description = "Cluster subnetwork"
}

variable "nat_subnetwork" {
  description = "NAT subnetwork"
}

variable "master_cidr" {
  description = "Master CIDR block"
}

variable "master_authorized_networks_cidr" {
  description = "Master authorized cidr blocks"
  type        = list

  default = [
    {
      cidr_block   = "10.0.0.0/8"
      display_name = "internal"
    }
  ]
}