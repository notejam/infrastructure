module "cluster" {
  source                          = "git::https://github.com/gadiener/terraform-modules.git//cluster?ref=v2.1.3"

  name                            = "production"
  zone                            = "${var.region}-${var.zone}"
  network                         = var.network
  subnetwork                      = "${var.subnetwork}"
  pods_range_name                 = "${var.subnetwork}-pods"
  services_range_name             = "${var.subnetwork}-services"
  master_ipv4_cidr_block 					= var.master_cidr

  # We need a VPN inside the network to enable private master
  enable_private_master 					= false
  master_authorized_networks_cidr = var.master_authorized_networks_cidr

  cluster_autoscaling = [
    {
      enabled = false
    },
  ]

  labels = {
    scope = "production"
    env   = "production"
  }
}

module "primary" {
  source         = "git::https://github.com/gadiener/terraform-modules.git//pool?ref=v2.1.3"

  name           = "primary"
  zone           = "${var.region}-${var.zone}"
  cluster        = module.cluster.name
  machine_type   = "n1-standard-2"
  node_count     = 1
  node_count_max = 5

  disk_type = "pd-standard"

  preemptible = true

  labels = {
    scope = "production"
    env   = "production"
  }
}

module "nat" {
  source = "git::https://github.com/gadiener/terraform-modules.git//nat?ref=v2.1.3"

  name          = "k8s-production-${var.region}-nat"
  region        = var.region
  network       = var.network
  subnetwork    = var.nat_subnetwork
  address_count = 1

  min_ports_per_vm = 4096

  enable_error_log = true
}
