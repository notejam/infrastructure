provider "google" {
  version     = "~> 2.9.0"
  region      = var.region
  project     = var.project
  credentials = file("../account.json")
}

provider "google-beta" {
  version     = "~> 2.9.0"
  region      = var.region
  project     = var.project
  credentials = file("../account.json")
}
