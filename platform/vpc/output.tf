output "network" {
  value = module.network.network
}

output "subnetwork" {
  value = module.production_subnetwork.subnetwork
}

output "network_link" {
  value = module.network.network_link
}

output "production_subnetwork_link" {
  value = module.production_subnetwork.subnetwork_link
}
