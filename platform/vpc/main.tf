module "network" {
  source = "git::https://github.com/gadiener/terraform-modules.git//network?ref=v2.1.3"

  name = "notejam"
}

module "production_subnetwork" {
  source = "git::https://github.com/gadiener/terraform-modules.git//subnetwork?ref=v2.1.3"

  name    = "production"
  region  = var.region
  network = module.network.network_link

  cidr_range = "10.10.0.0/16"

  secondary_ip_ranges = [
    {
      range_name    = "production-pods"
      ip_cidr_range = "10.11.0.0/16"
    },
    {
      range_name    = "production-services"
      ip_cidr_range = "10.12.0.0/16"
    },
  ]
}

module "firewall_from_vpn" {
  source      = "git::https://github.com/gadiener/terraform-modules.git//firewall/ingress-allow?ref=v2.1.3"
  name        = "vpn-access"
  description = "VPN access"
  network     = module.network.network_link

  allow = [
    { protocol = "udp" },
    { protocol = "tcp" },
  ]

  source_ranges = var.vpn_cidr
}
