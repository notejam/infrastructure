variable "region" {
  description = "Subnetwork region"
  type        = string
}

variable "vpn_cidr" {
  description = "Authorized vpn cidr"
  type        = list(string)

  default = []
}
