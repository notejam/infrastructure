variable "project" {
  description = "Project nameinit"
  type        = string
}

variable "region" {
  description = "Project region"
  type        = string
}

variable "zone" {
  description = "Project zone"
  type        = string
}

variable "vpn_cidr" {
  description = "Authorized vpn cidr"
  type        = list(string)
}

variable "database_password" {
  description = "Password for user notejam in the database"
  type        = string
}
