module "notejam_instance" {
  source = "./cloudsql"

  database_version = "MYSQL_5_7"
  name             = "notejam-production"
  region           = var.region

  disk_size        = "10"
  disk_type        = "PD_HDD"
  tier             = "db-g1-small"

  authorized_networks = concat(local.vpn_cidr_block, local.cluster_production_cidr_block)
}

resource "google_sql_database" "notejam_database" {
  name     = "notejam"
  instance = module.notejam_instance.name
}

resource "google_sql_user" "users" {
  name     = "notejam"
  instance = module.notejam_instance.name
  host     = "%"
  password = var.database_password
}