output "network" {
  value = module.network.network
}

output "cluster-production-name" {
  value = module.cluster-production.cluster
}
