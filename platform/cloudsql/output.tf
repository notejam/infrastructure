output "name" {
  value = google_sql_database_instance.database.name
}

output "address" {
  value = google_sql_database_instance.database.public_ip_address
}
