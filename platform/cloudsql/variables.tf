variable "name" {
  description = "the name of the db in GCP"
  type        = string
}

variable "region" {
  description = "the region of the db"
  type        = string
}

variable "database_version" {
  description = "the database_version in GCP"
  type        = string
}

variable "disk_autoresize" {
  description = "if disk_autoresize"
  type        = string

  default     = "false"
}

variable "disk_size" {
  description = "db disk size"
  type        = string

  default     = "10"
}

variable "disk_type" {
  description = "db disk type"
  type        = string

  default     = "PD_SSD"
}

variable "tier" {
  description = "the tier of the db"
  type        = string
}

variable "authorized_networks" {
  description = "the authorized_networks allowed to access the db"
  type        = list

  default = []
}
