resource "google_sql_database_instance" "database" {
  name             = var.name
  region           = var.region

  database_version = var.database_version

  settings {

    disk_autoresize        = var.disk_autoresize
    disk_size              = var.disk_size
    disk_type              = var.disk_type

    tier                   = var.tier

    ip_configuration {

      ipv4_enabled = "true"
      require_ssl  = "false"

      dynamic "authorized_networks" {

        for_each = var.authorized_networks

        content {
          name  = authorized_networks.value.display_name
          value = authorized_networks.value.cidr_block
        }
      }
    }
  }
}
