terraform {
  required_version = ">= 0.12"

  required_providers {
    google      = ">= 2.9.1"
    google-beta = ">= 2.9.1"
  }

  backend "gcs" {
    bucket      = "notejam-operations"
    prefix      = "terraform"
    credentials = "../account.json"
  }
}

module "network" {
  source = "./vpc"

  region    = var.region
  vpn_cidr  = var.vpn_cidr
}

locals {

  vpn_cidr_block = [
    for index, address in var.vpn_cidr : {
      display_name = "gateway-${index}"
      cidr_block = address
    }
  ]

  cluster_production_cidr_block = [
    for index, address in module.cluster-production.address : {
      display_name = "access-production-${index}"
      cidr_block = format("%s/32", address)
    }
  ]

  common_cidr_block = concat(
    [{
      cidr_block   = "10.0.0.0/8"
      display_name = "internal"
    }],
    local.vpn_cidr_block
  )
}