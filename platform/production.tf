module "cluster-production" {
  source = "./cluster"

  region         = var.region
  zone           = var.zone
  network        = module.network.network
  subnetwork     = module.network.subnetwork
  nat_subnetwork = module.network.production_subnetwork_link

  master_cidr    = "172.16.1.0/28"

  master_authorized_networks_cidr = local.common_cidr_block
}

module "cluster-production-access" {
  source = "git::https://github.com/gadiener/terraform-modules.git//firewall/ingress-allow?ref=v2.1.3"

  name        = "access-production"
  description = "K8s access from production"
  network     = module.network.network

  allow = [
    { protocol = "all" }
  ]

  source_ranges = formatlist("%s/32", module.cluster-production.address)
}

# Needed to use kubectl port-forward
module "firewall_from_masters" {
  source      = "git::https://github.com/gadiener/terraform-modules.git//firewall/ingress-allow?ref=v2.1.3"
  name        = "k8s-master-access"
  description = "K8s master node"
  network     = module.network.network_link

  allow = [
    { protocol = "udp" },
    { protocol = "tcp" },
  ]

  source_ranges = [
    "172.16.1.0/28"
  ]
}
